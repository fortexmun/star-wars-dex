import React, { useState, useEffect } from 'react';
import axios from 'axios';

function Characterpage() {
  //create useState variables
  //for the peoples/chracter details
  const [data, setData] = useState({ results: [] });
  //for input text
  const [query, setQuery] = useState('1');
  //for url link
  const [url, setUrl] = useState(
    'https://swapi.co/api/people/?page=' + query ,
    );
  //for loading effect
  const [isLoading, setIsLoading] = useState(false);
  //for error alert
  const [isError, setIsError] = useState(false);
  //use effect triggered when url is changed
  useEffect(() => {
    const fetchData = async () => {
      setIsError(false);
      setIsLoading(true);
      try {
      const result = await axios(url);

      setData(result.data);
    } catch (error) {
      setIsError(true);
    }
      setIsLoading(false);
    };

    fetchData();
  }, [url]);


  return (
    <characterPage>
    <div className="container-fluid">
      <h1>Characters</h1> 

      <p>Page(1-9) : {query}</p> 
      <input
        type="number"
        value={query}
        onChange={event => setQuery(event.target.value)}
      />
      
      <button type="button" onClick={() => 
        setUrl('https://swapi.co/api/people/?page='+query)
        }>
        Update
      </button>

      {isError && <div>Error!!! Enter value between 1 to 9</div>}

      {isLoading ? (
        <div>Loading Characters...</div>
      ) : (
      <ul>
      {data.results.map(item => (
        <li key={item.url}>
          <p>Name :  {item.name} 
          <br/>Height :  {item.height}
          <br/>Mass :  {item.mass}
          <br/>Hair Color :  {item.hair_color}
          <br/>Skin Color :  {item.skin_color}
          <br/>Eye Color :  {item.eye_color}
          <br/>Birth Year :  {item.birth_year}
          <br/>Gender :  {item.gender}
          </p>
          
        </li>
      ))}
      </ul>
      )}
    
    </div>
    </characterPage>
  );
}

export default Characterpage;
