import React from 'react';
import{
    Link
  } from 'react-router-dom';

//image
import greenSword from './headerImages/green-sword.png';
import starWarsLogo from './headerImages/Starwars-logo.png';

//Link function is for anchor text reference

function Header() {
  return (
    <header>
        <div className="logo1">
            <img src= {greenSword} alt="greenSword" />
        </div>
        <div className="logo2">
            <img src= {starWarsLogo} alt="starWarsLogo" />
        </div>
        <nav>
            <ul>
                <li className="home">
                    <Link to="/">Home</Link>
                </li>
                |
                <li className="char">
                    <Link to="/characters">Characters</Link>
                </li>
            </ul>
        </nav>
    </header>
  );
}

export default Header;
