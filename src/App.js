import React from 'react';
import{
  BrowserRouter as Router,
  Route
} from 'react-router-dom';

//includes
import "./Assets/css/default.min.css";

//components
import Header from './Components/headerComp/header';
import Footer from './Components/footerComp/footer';
import Homepage from './Components/pagesComp/homePage';
import Characterpage from './Components/pagesComp/characterPage';

function App() {
  return (
    <Router>
    <div className="App">
      <Header/>
      <Route exact path='/' component={Homepage}/>
      <Route exact path='/characters' component={Characterpage}/>

      <Footer/>
    </div>
    </Router>
  );
}

export default App;
